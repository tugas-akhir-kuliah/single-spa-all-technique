import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'nav-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  navItems = [
    {
      name: 'Dashboard',
      link: '/dashboard'
    },
    {
      name: 'Article Administration',
      link: '/article'
    },
    {
      name: 'Sales',
      link: '/sales'
    }
  ];
  isAuthenticated = false;
  currentUserChannel;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.currentUserChannel = new BroadcastChannel("currentUser");
    this.currentUserChannel.onmessage = (e) => {
      this.isAuthenticated = e.data !== undefined && e.data !== null;
      this.changeDetectorRef.detectChanges();
    };
  }

  route(url: string) {
    window.history.pushState(null, null, url);
  }

  ngOnDestroy() {
    this.currentUserChannel.close();
  }
}
