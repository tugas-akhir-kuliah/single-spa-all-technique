import React, { Suspense } from "react";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";

const Login = React.lazy(() => import('./components/login.component'));

export default function Root(props) {
  return (
    <Router>
      <Switch>
        <Route exact path="/authentication/login">
          <Suspense fallback={<div>Loading...</div>}>
            <Login />
          </Suspense>
        </Route>
      </Switch>
    </Router>
  );
}