import { registerApplication, start } from "single-spa";

registerApplication({
  name: "@tuwien/navigation",
  app: () => System.import("@tuwien/navigation"),
  activeWhen: ["/"],
});

start({
  urlRerouteOnly: true,
});

window.addEventListener("single-spa:first-mount", () => {
  registerApplication({
    name: "@tuwien/authentication",
    app: () => System.import("@tuwien/authentication"),
    activeWhen: ["/authentication"],
  });

  registerApplication({
    name: "@tuwien/dashboard",
    app: () => System.import("@tuwien/dashboard"),
    activeWhen: ["/dashboard"],
  });

  registerApplication({
    name: "@tuwien/article",
    app: () => System.import("@tuwien/article"),
    activeWhen: ["/article"],
  });

  registerApplication({
    name: "@tuwien/sales",
    app: () => System.import("@tuwien/sales"),
    activeWhen: ["/sales"],
  });
});
