import vue from 'rollup-plugin-vue';
import json from '@rollup/plugin-json';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';

export default {
  input: 'src/main.js', // Ubah sesuai dengan file utama Anda
  output: {
    file: 'dist/js/bundle.js', // Ubah sesuai dengan lokasi dan nama file output yang diinginkan
    format: 'iife', // Format output yang diinginkan, misalnya IIFE
    sourcemap: true,
  },
  plugins: [
    vue(),
    json(),
    nodeResolve(), // Plugin untuk memecahkan dependensi Node.js
    commonjs(), // Plugin untuk memungkinkan menggunakan modul CommonJS
  ],
};
